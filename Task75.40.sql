-- 1 Hiển thị toàn bộ thông tin khách hàng (customers) có credit > 50000
SELECT * FROM `customers` WHERE credit_limit>50000;
-- 2 Hiển thị toàn bộ thông tin danh sách sản phẩm, với tên của product_line tương ứng (không hiển thị product_line_id mà hiển thị tên product line)
SELECT products.product_code,products.product_name,products.id,products.product_description,products.product_scale,products.product_vendor,products.quantity_in_stock,products.buy_price ,product_lines.product_line FROM products INNER JOIN product_lines ON products.product_line_id=product_lines.id;
-- 3 Đếm xem ở từng nước, có bao nhiêu khách hàng? (hiển thị tên nước, số lượng khách hàng)
SELECT COUNT(customers.country) AS count_c ,customers.country FROM customers GROUP BY customers.country;
-- 4 Sắp xếp các sản phẩm theo giá mua tăng dần và lấy ra 10 sản phẩm đầu tiên
SELECT * FROM `products` ORDER BY products.buy_price DESC LIMIT 0,10;
-- 5 Lấy ra danh sách các đơn hàng được đặt vào tháng 10/2003
SELECT * FROM `orders` WHERE orders.order_date>"2003-10-01" AND orders.order_date<"2003-11-01";

-- 6 Lấy ra danh sách các khách hàng có số lượng đơn hàng > 3
SELECT COUNT(orders.customer_id) AS count_cus ,orders.id,orders.order_date,orders.required_date,orders.shipped_date,orders.status,orders.comments FROM orders GROUP BY(orders.customer_id) HAVING count_cus>3;
-- 7 Tính toán xem, mỗi đơn hàng có bao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu Hiển thị: order_id, tổng số lượng và tổng tiền

-- 8 Tính toán xem, mỗi đơn hàng có bsao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu. Và lấy ra danh sách các đơn có tổng tiền > 100000 Hiển thị: order_id, tổng số lượng và tổng tiền

-- 9 Tìm và hiển thị toàn bộ thông tin sản phẩm được đặt nhiều nhất (theo quantity_order) trong năm 2004
